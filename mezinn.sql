-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 12, 2018 at 04:01 PM
-- Server version: 10.0.36-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.2.11-2+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mezinn`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`) VALUES
(1, 'Phones', 'phones'),
(2, 'Cars', 'cars');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `slug`) VALUES
(1, 'Poltava', 'poltava');

-- --------------------------------------------------------

--
-- Table structure for table `dialogs`
--

CREATE TABLE `dialogs` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dialogs`
--

INSERT INTO `dialogs` (`id`, `service_id`) VALUES
(7, 6);

-- --------------------------------------------------------

--
-- Table structure for table `dialog_messages`
--

CREATE TABLE `dialog_messages` (
  `id` int(11) NOT NULL,
  `dialog_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dialog_messages`
--

INSERT INTO `dialog_messages` (`id`, `dialog_id`, `content`, `owner_id`) VALUES
(1, 7, 'sdfgsdfgsdfg', 7),
(2, 7, '228\r\n', 7),
(3, 7, '555', 6),
(4, 7, '2345', 7),
(5, 7, 'sdfgsdfkjngsdkjfgnskjfdngjsdfngjsfdng\r\n', 7),
(6, 7, 'sdfgsdfgsdfg', 7),
(7, 7, 'sdfgsdfgsdfg', 7),
(8, 7, 'sdfgsdfgsdfg', 7),
(9, 7, 'sdfg', 7),
(10, 7, 'wertwetwret', 7),
(11, 7, 'wertwertwertwret', 7),
(12, 7, '4563456346346', 7),
(13, 7, 'sdkjgbsdfkjgbjsgjksdfgbsfdbsdfg\r\n', 7),
(14, 7, 'fghdfghdfhdfh', 7),
(15, 7, 'dfghsdfgsdfgsdfgsdfg', 7),
(16, 7, 'sdfgsdfgsdfgsdfgsfg', 7),
(17, 7, 'sdfgsdfgsfdggsdfgsdfg', 7),
(18, 7, 'cghjfgjfgjfhjfghjfgj', 7),
(19, 7, '4563465463456', 7),
(20, 7, 'dhfghdfhdfh', 7),
(21, 7, 'dfhdghdhg', 7),
(22, 7, 'gfhsgfhdfghdfh', 7),
(23, 7, 'asdf', 7),
(24, 7, 'asdfgsdfg', 7);

-- --------------------------------------------------------

--
-- Table structure for table `favorite_services`
--

CREATE TABLE `favorite_services` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `owner_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favorite_services`
--

INSERT INTO `favorite_services` (`id`, `service_id`, `owner_Id`) VALUES
(1, 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `preview_image_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `category_id`, `owner_id`, `description`, `price`, `created_at`, `updated_at`, `city_id`, `is_active`, `preview_image_name`) VALUES
(6, 'Фотограф', 1, 6, '<p>Something</p>', '5000.000', '2018-11-12 11:48:16', '2018-11-12 11:48:16', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `phone_number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email_address`, `password_hash`, `auth_key`, `access_token`, `created_at`, `updated_at`, `phone_number`) VALUES
(6, NULL, NULL, 'goto@gmail.com', '$2y$13$jIdG4vSPgZ.fqQK.th.wEOSQb0G2iWhr20uOlW.sZaR.IP/xilmK2', '', '', '2018-11-12 10:54:33', '2018-11-12 10:54:33', NULL),
(7, NULL, NULL, 'mezinn@gmail.com', '$2y$13$lEDuYYx10pEXWx29M5PfzeVYtf79QUCJeOBqZzh.klM43aC/th8yS', '\'-', '', '2018-11-12 12:27:42', '2018-11-12 12:27:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_dialogs`
--

CREATE TABLE `user_dialogs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dialog_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_dialogs`
--

INSERT INTO `user_dialogs` (`id`, `user_id`, `dialog_id`) VALUES
(9, 6, 7),
(10, 7, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `dialogs`
--
ALTER TABLE `dialogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `dialog_messages`
--
ALTER TABLE `dialog_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dialog_id` (`dialog_id`),
  ADD KEY `owner_id` (`owner_id`);

--
-- Indexes for table `favorite_services`
--
ALTER TABLE `favorite_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `owner_Id` (`owner_Id`),
  ADD KEY `service_id_2` (`service_id`),
  ADD KEY `owner_Id_2` (`owner_Id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `owner_id` (`owner_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `owner_id_2` (`owner_id`),
  ADD KEY `city_id_2` (`city_id`),
  ADD KEY `category_id_2` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_dialogs`
--
ALTER TABLE `user_dialogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dialog_id` (`dialog_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dialogs`
--
ALTER TABLE `dialogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dialog_messages`
--
ALTER TABLE `dialog_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `favorite_services`
--
ALTER TABLE `favorite_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_dialogs`
--
ALTER TABLE `user_dialogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dialogs`
--
ALTER TABLE `dialogs`
  ADD CONSTRAINT `dialogs_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION;

--
-- Constraints for table `dialog_messages`
--
ALTER TABLE `dialog_messages`
  ADD CONSTRAINT `dialog_messages_ibfk_1` FOREIGN KEY (`dialog_id`) REFERENCES `dialogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `dialog_messages_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorite_services`
--
ALTER TABLE `favorite_services`
  ADD CONSTRAINT `favorite_services_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favorite_services_ibfk_2` FOREIGN KEY (`owner_Id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `services_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `user_dialogs`
--
ALTER TABLE `user_dialogs`
  ADD CONSTRAINT `user_dialogs_ibfk_1` FOREIGN KEY (`dialog_id`) REFERENCES `dialogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_dialogs_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
