<?php

namespace app\helpers\users;

use Yii;
use Exception;
use app\models\Users;
use yii\base\Component;
use app\forms\user\UserForm;
use yii\web\NotFoundHttpException;

/**
 * Description of UserHelper
 *
 * @author mezinn
 */
class UserHelper extends Component {

    public function createNew(UserForm $form) {
        $model = new Users([
            'email_address' => $form->getEmailAddress(),
            'password_hash' => Yii::$app->security->generatePasswordHash($form->getPassword()),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'access_token' => Yii::$app->security->generateRandomString(),
        ]);
        return $model->save();
    }

    public function login(UserForm $form) {
        try {
            $model = $this->findIdentityByEmailAddress($form->getEmailAddress());
            if (Yii::$app->security->validatePassword($form->getPassword(), $model->password_hash) and Yii::$app->user->login($model)) {
                return true;
            }
        } catch (Exception $ex) {
            
        }
        $form->pushErrors('Incorrect username or password');
        return false;
    }

    public function findIndetityById(int $id) {
        if (($model = Users::findOne($id))) {
            return $model;
        }
        throw new NotFoundHttpException();
    }

    public function findIdentityByEmailAddress(string $email_address) {
        if (($model = Users::findOne(['email_address' => $email_address]))) {
            return $model;
        }
        throw new NotFoundHttpException();
    }

    public function findIdentityByAccessToken(string $access_token) {
        if (($model = Users::findOne(['access_token' => $access_token]))) {
            return $model;
        }
        throw new NotFoundHttpException();
    }

}
