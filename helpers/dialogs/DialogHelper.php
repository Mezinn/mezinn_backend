<?php

namespace app\helpers\dialogs;

use Yii;
use yii\base\Component;
use app\models\Dialogs;
use app\models\UserDialogs;

/**
 * Description of DialogHelper
 *
 * @author mezinn
 */
class DialogHelper extends Component {

    public function createNew($service_id, $owner_id, $user_id) {
        $dialog = new Dialogs(['service_id' => $service_id]);
        if ($dialog->save() and ( new UserDialogs(['user_id' => $owner_id, 'dialog_id' => $dialog->id]))->save()and ( new UserDialogs(['user_id' => $user_id, 'dialog_id' => $dialog->id]))->save()) {
            return true;
        }
        throw new \yii\web\ServerErrorHttpException();
    }

    public function top() {
        $model = UserDialogs::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->limit(1)->one();
        return $model->dialog;
    }

    public function exists($user_id, $service_id) {
        return Dialogs::find()->innerJoinWith('userDialogs')->where(['dialogs.service_id' => $service_id, 'user_dialogs.user_id' => $user_id])->limit(1)->one() !== null;
    }

    public function find($user_id, $service_id) {
        return Dialogs::find()->innerJoinWith('userDialogs')->where(['dialogs.service_id' => $service_id, 'user_dialogs.user_id' => $user_id])->limit(1)->one();
    }

}
