<?php

namespace app\helpers\cities;

use app\models\Cities;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Description of CitiesHelper
 *
 * @author mezinn
 */
class CityHelper extends Component {

    public function getList() {
        return ArrayHelper::map(Cities::find()->all(), 'id', 'name');
    }

}
