<?php

namespace app\helpers\services;

use Yii;
use yii\base\Component;
use app\models\Services;
use app\models\FavoriteServices;
use yii\web\NotFoundHttpException;

/**
 * Description of ServiceHelper
 *
 * @author mezinn
 */
class ServiceHelper extends Component {

    public function findModel($id) {
        if (($model = Services::findOne($id))) {
            return $model;
        }
        throw new NotFoundHttpException();
    }

    public function toggleFavorite($id) {
        if (($model = FavoriteServices::findOne(['owner_Id' => Yii::$app->user->id, 'service_id' => $id]))) {
            return $model->delete();
        } else {
            return (new FavoriteServices(['owner_Id' => Yii::$app->user->id, 'service_id' => $id]))->save();
        }
    }

    public function isFavorite($id) {
        return FavoriteServices::findOne(['owner_Id' => Yii::$app->user->id, 'service_id' => $id]) !== null;
    }

}
