<?php

namespace app\helpers\categories;

use yii\base\Component;
use app\models\Categories;
use yii\helpers\ArrayHelper;

/**
 * Description of CategoryHelper
 *
 * @author mezinn
 */
class CategoryHelper extends Component {

    public function getList() {
        return ArrayHelper::map(Categories::find()->all(), 'id', 'name');
    }

}
