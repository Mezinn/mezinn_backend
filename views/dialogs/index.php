<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\DialogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Діалоги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dialogs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'service.name',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->service->name, ['view', 'id' => $model->id]);
                },
            ],
            [
                'label' => 'Електронна пошта',
                'attribute' => 'service.owner.email_address',
            ],
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
