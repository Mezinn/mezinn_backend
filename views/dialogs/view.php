<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Dialogs */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Діалоги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dialogs-view">

    <h3><?= Html::encode("Діалог з {$model->service->owner->email_address} / Послуга (") . Html::a($model->service->name, ['services/view', 'id' => $model->service_id]) . ")" ?></h3>

</div>
<?php Pjax::begin(['id' => 'chat']) ?>
<?= ListView::widget(['dataProvider' => $dataProvider, 'itemView' => '_message', 'layout' => '{items}{pager}']) ?>
<?php Pjax::end() ?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($new_message, 'content')->textarea(['rows' => 6])->label(false) ?>
<?= Html::submitButton('Відправити', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end(); ?>
<script>
    setTimeout(function () {
        $.pjax.reload({container: '#chat', async: false});
    }
    , 5000)
</script>