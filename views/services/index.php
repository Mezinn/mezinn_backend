<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\widgets\ServicesWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Послуги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ServicesWidget::widget() ?>
    <?php Pjax::begin(['id' => 'index']); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->name, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'category_id',
                'value' => 'category.name',
                'filter' => Yii::$app->categoryHelper->getList(),
            ],
            'price:integer',
            [
                'attribute' => 'city_id',
                'value' => 'city.name',
                'filter' => Yii::$app->cityHelper->getList(),
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{send-message}{toggle-favorite}',
                'buttons' => [
                    'send-message' => function($uri, $model) {
                        if (!($dialog = Yii::$app->dialogHelper->find(Yii::$app->user->id, $model->id))) {
                            return Html::a('', ['dialogs/create', 'service_id' => $model->id], ['class' => 'glyphicon glyphicon-envelope']);
                        } else {
                            return Html::a('', ['dialogs/view', 'id' => $dialog->id], ['class' => 'glyphicon glyphicon-envelope']);
                        }
                    },
                    'toggle-favorite' => function($uri, $model) {
                        return Html::a('', ['services/toggle-favorite', 'service_id' => $model->id], ['class' => Yii::$app->serviceHelper->isFavorite($model->id) ? 'glyphicon glyphicon-star' : 'glyphicon glyphicon-star-empty', 'onclick' => 'toggle_favorite(event)', 'data-id' => $model->id]);
                    }
                ]
            ],
        ],
    ]);
    ?>


    <?php Pjax::end(); ?>
</div>

<script>
    function toggle_favorite(event) {
        event.preventDefault();
        $.ajax({
            url: '/services/toggle-favorite',
            data: {service_id: event.target.dataset.id},
            success: function () {
                $.pjax.reload({container: '#index'});
            }
        });
    }

</script>