<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\widgets\ServicesWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мої послуги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ServicesWidget::widget() ?>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('Додати', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'category_id',
                'value' => 'category.name',
                'filter' => Yii::$app->categoryHelper->getList(),
            ],
            'price:integer',
            [
                'attribute' => 'city_id',
                'value' => 'city.name',
                'filter' => Yii::$app->cityHelper->getList(),
            ],
            'is_active:boolean',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>


    <?php Pjax::end(); ?>
</div>
