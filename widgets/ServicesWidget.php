<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\widgets\Menu;

/**
 * Description of ServicesWidget
 *
 * @author mezinn
 */
class ServicesWidget extends Widget {

    public function run() {
        return Menu::widget([
                    'items' => [
                        ['label' => 'Послуги', 'url' => ['services/index'],'visible' => !Yii::$app->user->isGuest],
                        ['label' => 'Мої послуги', 'url' => ['services/self'], 'visible' => !Yii::$app->user->isGuest],
                        ['label' => 'Закладки', 'url' => ['services/favorites'], 'visible' => !Yii::$app->user->isGuest],
                    ],
                    'options' => ['class' => 'service-tabs'],
        ]);
    }

}
