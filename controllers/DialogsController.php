<?php

namespace app\controllers;

use Yii;
use app\models\Dialogs;
use app\models\search\DialogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\DialogMessages;

/**
 * DialogsController implements the CRUD actions for Dialogs model.
 */
class DialogsController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new DialogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {

        $model = $this->findModel($id);
        $new_message = new DialogMessages(['dialog_id' => $model->id, 'owner_id' => Yii::$app->user->id]);
        if ($new_message->load(Yii::$app->request->post()) and $new_message->save()) {
            return $this->refresh();
        }
        Yii::error(print_r($new_message->errors, true));
        return $this->render('view', [
                    'model' => $model,
                    'new_message' => $new_message,
                    'dataProvider' => $model->messagesDataProvider,
        ]);
    }

    public function actionCreate($service_id) {
        $service = Yii::$app->serviceHelper->findModel($service_id);
        if (Yii::$app->getDb()->transaction(function() use ($service) {
                    return Yii::$app->dialogHelper->createNew($service->id, $service->owner_id, Yii::$app->user->id);
                })) {
            return $this->redirect(['/dialogs/view', 'id' => Yii::$app->dialogHelper->top()->id]);
        }
        return $this->goBack();
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Dialogs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
