<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dialog_messages".
 *
 * @property int $id
 * @property int $dialog_id
 * @property string $content
 *
 * @property Dialogs $dialog
 */
class DialogMessages extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'dialog_messages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['dialog_id', 'content', 'owner_id'], 'required'],
            [['dialog_id'], 'integer'],
            [['content'], 'string'],
            [['dialog_id'], 'exist', 'targetClass' => Dialogs::className(), 'targetAttribute' => ['dialog_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'dialog_id' => 'Dialog ID',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialog() {
        return $this->hasOne(Dialogs::className(), ['id' => 'dialog_id']);
    }

}
