<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "dialogs".
 *
 * @property int $id
 * @property int $service_id
 *
 * @property DialogMessages[] $dialogMessages
 * @property Services $service
 * @property UserDialogs[] $userDialogs
 */
class Dialogs extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'dialogs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['service_id'], 'required'],
            [['service_id'], 'integer'],
            [['service_id'], 'exist', 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialogMessages() {
        return $this->hasMany(DialogMessages::className(), ['dialog_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService() {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDialogs() {
        return $this->hasMany(UserDialogs::className(), ['dialog_id' => 'id']);
    }

    public function getMessagesDataProvider() {




        return new ActiveDataProvider(['query' =>
//            DialogMessages::find()->where(['dialog_id' => $this->id])])
            DialogMessages::findBySql("SELECT * FROM (SELECT * FROM `dialog_messages` WHERE dialog_id = {$this->id} ORDER BY id DESC) as base ORDER BY ID ASC"),
                    'pagination'=>[
                        'defaultPageSize'=>DialogMessages::findBySql("SELECT * FROM (SELECT * FROM `dialog_messages` WHERE dialog_id = {$this->id} ORDER BY id DESC) as base ORDER BY ID ASC")->count(),
                    ]
        ]);
    }

}
