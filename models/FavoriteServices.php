<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "favorite_services".
 *
 * @property int $id
 * @property int $service_id
 * @property int $owner_Id
 */
class FavoriteServices extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'favorite_services';
    }

    public function rules() {
        return [
            [['service_id', 'owner_Id'], 'required'],
            [['service_id', 'owner_Id'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'owner_Id' => 'Owner  ID',
        ];
    }

    public function getOwner() {
        return $this->hasOne(Users::className(), ['owner_Id' => 'id']);
    }

    public function getService() {
        return $this->hasOne(Services::className(), ['service_id' => 'id']);
    }

}
