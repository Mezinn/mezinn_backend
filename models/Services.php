<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "services".
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 * @property int $owner_id
 * @property string $description
 * @property string $price
 * @property string $created_at
 * @property string $updated_at
 * @property int $city_id
 * @property int $is_active
 * @property string $preview_image_name
 */
class Services extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'services';
    }

    public function behaviors() {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function rules() {
        return [
            [['name', 'category_id', 'owner_id', 'description', 'price', 'city_id', 'is_active'], 'required'],
            [['category_id', 'owner_id', 'city_id', 'is_active'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['preview_image_name'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'category_id' => 'Категорія',
            'owner_id' => 'Owner ID',
            'description' => 'Інформація',
            'price' => 'Ціна',
            'created_at' => 'Опубліковано',
            'updated_at' => 'Updated At',
            'city_id' => 'Місто',
            'is_active' => 'Активно',
        ];
    }

    public function getCity() {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    public function getOwner() {
        return $this->hasOne(Users::className(), ['id' => 'owner_id']);
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public function getFavorites() {
        return $this->hasMany(FavoriteServices::className(), ['service_id' => 'id']);
    }

}
