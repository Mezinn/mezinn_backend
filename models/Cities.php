<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "cities".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 */
class Cities extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'cities';
    }

    public function behaviors() {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ]
        ];
    }

    public function rules() {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['name'], 'unique'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
        ];
    }

}
