<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $email_address
 * @property string $password_hash
 * @property string $auth_key
 * @property string $access_token
 * @property string $created_at
 * @property string $updated_at
 * @property string $phone_number
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface {

    public static function tableName() {
        return 'users';
    }

    public function behaviors() {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function rules() {
        return [
            [['email_address', 'password_hash', 'auth_key', 'access_token'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'surname', 'email_address', 'auth_key', 'access_token'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 32],
            [['password_hash'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'email_address' => 'Email Address',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'phone_number' => 'Phone Number',
        ];
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey){
        return $this->auth_key === $authKey;
    }

    public static function findIdentity($id) {
        return self::findOne($id) ?: null;
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return self::findOne(['access_token' => $token]) ?: null;
    }

    public function getServices() {
        return $this->hasMany(Services::className(), ['owner_id' => 'id']);
    }

}
