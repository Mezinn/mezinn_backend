<?php

namespace app\forms\user;

use yii\base\Model;
use app\models\Users;

/**
 * Description of RegisterForm
 *
 * @author mezinn
 */
class RegisterForm extends Model implements UserForm {

    public $email_address;
    public $password;
    public $confirm_password;

    public function rules() {
        return [
            [['email_address', 'password', 'confirm_password'], 'required'],
            [['email_address', 'password', 'confirm_password'], 'string', 'max' => 255],
            [['email_address'], 'unique', 'targetClass' => Users::className(), 'targetAttribute' => 'email_address'],
            [['confirm_password'], 'compare', 'compareAttribute' => 'password'],
            [['email_address'], 'email'],
        ];
    }

    public function attributeLabels() {
        return [
            'email_address' => 'Електронна пошта',
            'password' => 'Пароль',
            'confirm_password' => 'Повтор пароля'
        ];
    }

    public function getEmailAddress() {
        return $this->email_address;
    }

    public function getPassword() {
        return $this->password;
    }

    public function pushErrors($error) {
        
    }

}
