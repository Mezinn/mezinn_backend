<?php

namespace app\forms\user;

/**
 * Description of UserForm
 *
 * @author mezinn
 */
interface UserForm {

    public function getEmailAddress();

    public function getPassword();

    public function pushErrors($error);
}
