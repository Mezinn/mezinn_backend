<?php

namespace app\forms\user;

use yii\base\Model;

/**
 * Description of LoginForm
 *
 * @author mezinn
 */
class LoginForm extends Model implements UserForm {

    public $email_address;
    public $password;
    public $rememberMe;

    public function rules() {
        return [
            [['email_address', 'password'], 'required'],
            [['email_address', 'password'], 'string'],
            [['rememberMe'], 'boolean'],
        ];
    }

    public function attributeLabels() {
        return [
            'email_address' => 'Електронна пошта',
            'password' => 'Пароль',
            'rememberMe' => 'Запам\'ятати',
        ];
    }

    public function getEmailAddress() {
        return $this->email_address;
    }

    public function getPassword() {
        return $this->password;
    }

    public function pushErrors($error) {
        $this->addError('password', $error);
    }

}
